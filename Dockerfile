# Étape 1: Base Image
FROM node:latest

# Étape 2: Environnement de Travail
WORKDIR /usr/src/app

# Étape 3: Copie des Fichiers
COPY package*.json ./

# Étape 4: Installation des Dépendances
RUN npm install

# Copie du reste des fichiers de l'application
COPY . .

# Étape 5: Exposition du Port
EXPOSE 3000

# Étape 6: Commande de Démarrage
CMD ["npm", "start"]