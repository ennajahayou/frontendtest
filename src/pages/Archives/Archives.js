import React, { useState, useEffect, useContext , useRef} from "react";
import "./Archives.css";
// import Sidebar from "../../../Components/Sidebar";
import Sidebar from "../../Components/SidebarDIO.js";
import PeerReview from "../ExecutionBoard/PeerReview";
import SelfReview from "../DIOhomepage/SelfReview";
import axios from "axios";
import CEOReview from "../CEOProfil/CEOReview";
import Wallet from "../../Components/Wallet";
import ExecutionMessaging from "../DIOhomepage/ExecutionMessaging";
import SubmitionPopUp from "../DIOhomepage/PopUp/SubmitionPopUp";

import WorkDonePopUp from "../DIOhomepage/PopUp/WorkDonePopUp";
import Work from "../DIOhomepage/PopUp/Work";
import AttributionPopUp from "../DIOhomepage/PopUp/AttributionPopUp";
import AddLink from "../DIOhomepage/PopUp/AddLink";
import ExecutionInProgress from "../DIOhomepage/FeedCard/ExecutionInProgress";
import ExecutionInReview from "../DIOhomepage/FeedCard/ExecutionSelfPerfo";
import ExecutionNotAssigned from "../DIOhomepage/FeedCard/ExecutionNotAssigned";
import CEOreviewPopUp from "../DIOhomepage/PopUp/CEOreviewPopUp";
import ExecutionAttribution from "../DIOhomepage/ExecutionAttribution";
import ExecutionCreation from "../DIOhomepage/ExecutionCreation";


import CEONotYet from "../CEOProfil/CEONotYet";
import CEORejected from "../CEOProfil/CEORejected";
import PeerReviewNotYet from "../ExecutionBoard/PeerReviewNotYet";
import PeerReviewPopUp from "../DIOhomepage/PopUp/PeerReviewPopUp";


import { TasksContext } from "../TasksContext";




const Archives = () => {

  const { dioTasks } = useContext(TasksContext);
  const { propositions } = useContext(TasksContext);
  const { prop } = useContext(TasksContext);

  const [showPopUp, setShowPopUp] = useState(false);
  const [showPopUpWorkDone, setShowPopUpWorkDone] = useState(false);
  const [showPopUpWork, setShowPopUpWork] = useState(false);
  const [showPopUpLink, setShowPopUpLink] = useState(false);
  const [showPopUpAttribution, setShowPopUpAttribution] = useState(false);
  const [createExecutionText, setCreateExecutionText] = useState("");
  const [WorkText, setWorkText] = useState("");
  const [CommentCEO, setCommentCEO] = useState("");

  let [currentExecution, setCurrentExecution] = useState(null);

  const [showPopUpCEO, setShowPopUpCEO] = useState(false);
  const [ceoReview, setCeoReview] = useState(false);
  const [executionId, setExecutionId] = useState(0);
  const [isAttributingExecution, setIsAttributingExecution] = useState(false);
  const [isCreatingExecution, setIsCreatingExecution] = useState(false);
  const [
    creationExecutionWorkAlreadyDone,
    setCreationExecutionWorkAlreadyDone,
  ] = useState(false);

  const [feedbackCEO, setFeedbackCEO] = useState("");
  const [link, setLink] = useState(""); // State to store link value
  let [name, setName] = useState(null);
  const [ceoNotYet, setCeoNotYet] = useState(false);
  const [ceoRejected, setRejected] = useState(false);
  const [peerReviewNotYet, setpeerReviewNotYet] = useState(false);
  const [showPeerReview, setShowPeerReview] = useState(false);

  let [linkto, setLinkto] = useState(null);


  const [showPopUpPeerReview, setShowPopUpPeerReview] = useState(false);
  const [CommentPeerReview, setCommentPeerReview] = useState("");

  let handlePeerReviewClick = (executionId, name ,link) => {
    setShowPopUpPeerReview(true);
    setCurrentExecution(executionId);
    setName(name);
    setLinkto(link)
  };
  let handleCEOReview = (executionId , name ,link) => {
    setCurrentExecution(executionId); // Set the ID of the execution card clicked
    setShowPopUpCEO(true);
    setName(name);
    setLinkto(link) // Toggle the CEO review popup
  };

  const myDivRef = useRef(null);
  

  useEffect(() => {
    if (myDivRef.current) {
      myDivRef.current.scrollTop = myDivRef.current.scrollHeight - myDivRef.current.clientHeight;
    }
  }, [myDivRef.current]);

  const [selectedStatus, setSelectedStatus] = useState("All"); // Initialize with "All" or any default value

  const handleStatusChange = (event) => {
    setSelectedStatus(event.target.value);
  };

  // Filter executions based on the selected status
  const filteredExecutions = dioTasks.filter((execution) => {
    const userName = localStorage.getItem('userName');
    const userId = localStorage.getItem('userId');
    if (selectedStatus === "All") {
      return execution.talent_name === userName ; // Show all executions if "All" is selected
    } else if(selectedStatus === "My Ongoing"){
      return execution.talent_name === userName && execution.status_ === "On going";
    } else if(selectedStatus === "My Achieved" ){
      return execution.status_ ==="Achieved" && execution.talent_name === userName 
    } else if(selectedStatus === "My Rejected"){
      return execution.status_ ==="Rejected" && execution.talent_name === userName 
    } else {
      return execution.status_ === selectedStatus;
    }
  });

  filteredExecutions.sort((a, b) => a.id - b.id);

  const feed = filteredExecutions.map((execution) => {
    switch (execution.status_) {
      case "Not assigned":
        return (
          <ExecutionNotAssigned
            id={execution.id}
            description={execution.exec_description}
            talent={execution.talent_name}
            setExecutionId={setExecutionId}
            setShowPopUpAttribution={setShowPopUpAttribution}
          />
        );
      case "In progress":
        return (
          <ExecutionInProgress
            id={execution.id}
            description={execution.exec_description}
            talent={execution.talent_name}
            deadline={execution.deadline}
          />
        );
        case "Open":
          return (
            <ExecutionInReview
              id={execution.id}
              description={execution.exec_description}
              talent={execution.talent_name}
              status={execution.status_}
              comments={execution.review_comments}
              selfDifficulty ={execution.review_difficulty}
              selfReactivity ={execution.review_reactivity}
              clickreview={() => handlePeerReviewClick(execution.id,execution.talent_name ,execution.link)}
              showceopop={() => handleCEOReview(execution.id ,execution.talent_name ,execution.link) }
              currentExecution={() =>{ setCurrentExecution(execution.id)}}
              ceo_comments={execution.ceo_comments}
              ceo_expectations={execution.ceo_expectations}
              ceo_reactivity={execution.ceo_reactivity}
              remainingTime={execution.remaining_time}
              link={execution.link}
              achievement_date={execution.creation_date}
              thanks={execution.score_thanks}
              peerReview_comments={execution.peer_review_comments}
              peerReviewers_ids={execution.peer_review_ids}
              ceoFeedback_notYet={execution.ceo_feedback_notYet}
              ceoFeedback_rejected= {execution.ceo_feedback_rejected}
              peerReview_notYet_comments= {execution.peerReview_feedback_notYet}
            />
          );
          case "Rejected":
            return (
              <ExecutionInReview
                id={execution.id}
                description={execution.exec_description}
                talent={execution.talent_name}
                status={execution.status_}
                comments={execution.review_comments}
                selfDifficulty ={execution.review_difficulty}
                selfReactivity ={execution.review_reactivity}
                clickreview={() => handlePeerReviewClick(execution.id,execution.talent_name ,execution.link)}
                showceopop={() => handleCEOReview(execution.id ,execution.talent_name ,execution.link) }
                currentExecution={() =>{ setCurrentExecution(execution.id)}}
                ceo_comments={execution.ceo_comments}
                ceo_expectations={execution.ceo_expectations}
                ceo_reactivity={execution.ceo_reactivity}
                remainingTime={execution.remaining_time}
                link={execution.link}
                achievement_date={execution.creation_date}
                thanks={execution.score_thanks}
                peerReview_comments={execution.peer_review_comments}
                peerReviewers_ids={execution.peer_review_ids}
                peerReview_expectations ={execution.peer_review_expectations} 
                peerReview_reactivity= {execution.peer_review_reactivity}
                ceoFeedback_notYet={execution.ceo_feedback_notYet}
                ceoFeedback_rejected= {execution.ceo_feedback_rejected}
                peerReview_notYet_comments= {execution.peerReview_feedback_notYet}
              />
            );
            case "On going":
              return (
                <ExecutionInReview
                  id={execution.id}
                  description={execution.exec_description}
                  talent={execution.talent_name}
                  status={execution.status_}
                  comments={execution.review_comments}
                  selfDifficulty ={execution.review_difficulty}
                  selfReactivity ={execution.review_reactivity}
                  clickreview={() => handlePeerReviewClick(execution.id,execution.talent_name ,execution.link)}
                  showceopop={() => handleCEOReview(execution.id ,execution.talent_name ,execution.link) }
                  currentExecution={() =>{ setCurrentExecution(execution.id)}}
                  ceo_comments={execution.ceo_comments}
                  ceo_expectations={execution.ceo_expectations}
                  ceo_reactivity={execution.ceo_reactivity}
                  remainingTime={execution.remaining_time}
                  link={execution.link}
                  achievement_date={execution.creation_date}
                  thanks={execution.score_thanks}
                  peerReview_comments={execution.peer_review_comments}
                  peerReviewers_ids={execution.peer_review_ids}
                  peerReview_expectations ={execution.peer_review_expectations} 
                  peerReview_reactivity= {execution.peer_review_reactivity}
                  ceoFeedback_notYet={execution.ceo_feedback_notYet}
                  ceoFeedback_rejected= {execution.ceo_feedback_rejected}
                  peerReview_notYet_comments= {execution.peerReview_feedback_notYet}
                />
          );
      case "In review":
        return (
          <ExecutionInReview
            id={execution.id}
            description={execution.exec_description}
            talent={execution.talent_name}
            status={execution.status_}
            comments={execution.review_comments}
            selfDifficulty ={execution.review_difficulty}
            selfReactivity ={execution.review_reactivity}
            clickreview={() => handlePeerReviewClick(execution.id,execution.talent_name ,execution.link)}
            showceopop={() => handleCEOReview(execution.id ,execution.talent_name ,execution.link) }
            currentExecution={() =>{ setCurrentExecution(execution.id)}}
            ceo_comments={execution.ceo_comments}
            ceo_expectations={execution.ceo_expectations}
            ceo_reactivity={execution.ceo_reactivity}
            remainingTime={execution.remaining_time}
            link={execution.link}
            achievement_date={execution.creation_date}
            thanks={execution.score_thanks}
            peerReview_comments={execution.peer_review_comments}
            peerReviewers_ids={execution.peer_review_ids}
            peerReview_expectations ={execution.peer_review_expectations} 
            peerReview_reactivity= {execution.peer_review_reactivity}
            ceoFeedback_notYet={execution.ceo_feedback_notYet}
            ceoFeedback_rejected= {execution.ceo_feedback_rejected}
            peerReview_notYet_comments= {execution.peerReview_feedback_notYet}
          />
        );
        case "Achieved":
          return (
            <ExecutionInReview
              id={execution.id}
              description={execution.exec_description}
              talent={execution.talent_name}
              status={execution.status_}
              comments={execution.review_comments}
              selfDifficulty ={execution.review_difficulty}
              selfReactivity ={execution.review_reactivity}
              clickreview={() => handlePeerReviewClick(execution.id,execution.talent_name ,execution.link)}
              showceopop={() => handleCEOReview(execution.id) }
              currentExecution={() =>{ setCurrentExecution(execution.id)}}
              ceo_comments={execution.ceo_comments}
              ceo_expectations={execution.ceo_expectations}
              ceo_reactivity={execution.ceo_reactivity}
              link={execution.link}
              achievement_date={execution.creation_date}
              thanks={execution.score_thanks}
              peerReview_comments={execution.peer_review_comments}
              peerReviewers_ids={execution.peer_review_ids}
              peerReview_expectations ={execution.peer_review_expectations} 
              peerReview_reactivity= {execution.peer_review_reactivity}
              ceoFeedback_notYet={execution.ceo_feedback_notYet}
              ceoFeedback_rejected= {execution.ceo_feedback_rejected}
              peerReview_notYet_comments= {execution.peerReview_feedback_notYet}
            />
          );
      default:
        return <></>;
    }
});

  return (
    <div className="App">
      <Sidebar/>
      {isAttributingExecution ? (
        <ExecutionAttribution
          executionId={executionId}
          setIsAttributingExecution={setIsAttributingExecution}
        />
      ) : isCreatingExecution ? (
        <ExecutionCreation
          executionDescription={createExecutionText}
          setIsCreatingExecution={setIsCreatingExecution}
        />

      ) : creationExecutionWorkAlreadyDone ? (
        <SelfReview
          executionDescription={createExecutionText}
          setShowEvaluation={setCreationExecutionWorkAlreadyDone}
          executionId={executionId}
          executionComment={WorkText}
          executionLink={link}
        />):
        showPeerReview ? (
          <PeerReview
            executionId={currentExecution}
            setShowPeerReview={setShowPeerReview}
          />
      ) : ceoReview ? (
        <CEOReview
          executionId={currentExecution}
          setShowEvaluation={setCeoReview}
          comments={CommentCEO}
        /> 
      ): ceoNotYet ? (  
        <CEONotYet
          executionId={currentExecution}
          feedback={feedbackCEO}
          setFeedback={setFeedbackCEO}
        /> ):ceoRejected ? (
          <CEORejected
          executionId={currentExecution}
          feedback={feedbackCEO}
          setFeedback={setFeedbackCEO}
          />
        ):peerReviewNotYet ?(
        <PeerReviewNotYet
          executionId={currentExecution}
          feedback={feedbackCEO}
          setFeedback={setFeedbackCEO}
        />
      ) : (
        <div className="main-content">
                    <Wallet  />
          <div className="execution-board">
          <h1>DTO Thanks C.I.T Cash</h1>
          <div>
        {/* Select button to choose status */}

          {localStorage.getItem("isCEO") === "1" ? (   
        <select value={selectedStatus} onChange={handleStatusChange}>
        <option value="All">All mine</option>
        <option value="My Ongoing">My Ongoing</option>
        <option value="My Achieved">My Achieved</option>
        <option value="My Rejected">My Rejected</option>
        <option value="My Validated">My Validated</option>
        <option value="My Refused">My Refused</option>
        </select>
        ) : (
          <select value={selectedStatus} onChange={handleStatusChange}>
        <option value="All">All mine</option>
        <option value="My Ongoing">My Ongoing</option>
        <option value="My Achieved">My Achieved</option>
        <option value="My Rejected">My Rejected</option>
        <option value="My Validated">My Validated</option>
        <option value="My Refused">My Refused</option>
          </select>
      )}

      </div>
          </div>
          <div className="execution-container">
          <div className="messages"  ref={myDivRef}  style={{
              height: '62vh', // Adjust height as needed
              overflowY: 'scroll',
              marginBottom:'0px',
              marginLeft: '2.2vw',
              paddingRight: '2.8vw',
              scrollbarWidth: '20px', // Largeur de la barre de défilement
              scrollbarColor: '#ccc #f4f4f4'
               // Optional, might not work in all browsers
        }}>{feed}</div>
        </div>
        <ExecutionMessaging
              createExecutionText={createExecutionText}
              setCreateExecutionText={setCreateExecutionText}
              setShowPopUp={setShowPopUp}
            />
          {/* Fin Messaging */}
          {showPopUp && (
            <SubmitionPopUp
              executionDescription={createExecutionText}
              dioId="1"
              setShowPopUp={setShowPopUp}
              setShowPopUpWorkDone={setShowPopUpWorkDone}
            />
          )}
          {showPopUpWorkDone && (
            <WorkDonePopUp
              setShowPopUpWorkDone={setShowPopUpWorkDone}
              setShowPopUpWork={setShowPopUpWork}
            />
          )}
          {showPopUpWork && (
            <Work
            setShowPopUpLink={setShowPopUpLink}
              setShowPopUpWork={setShowPopUpWork}
              setSelfReview={setCreationExecutionWorkAlreadyDone}
              initialWorkText={WorkText}
              setWorkText={setWorkText}
            />
          )}
          {showPopUpLink && (
            <AddLink
              setShowPopUpLink={setShowPopUpLink}
              setSelfReview={setCreationExecutionWorkAlreadyDone}
              setExecutionId={setExecutionId}
              initialWorkText={WorkText}
              setWorkText={setWorkText}
              link={link}
              setLink={setLink}
            />
          )}
          {showPopUpAttribution && (
            <AttributionPopUp
              setIsAttributingExecution={setIsAttributingExecution}
              setShowPopUpAttribution={setShowPopUpAttribution}
              setSelfReview={setCreationExecutionWorkAlreadyDone}
            />
          )}
          {showPopUpCEO && ( 
           <CEOreviewPopUp
           setShowPopUpCEO={setShowPopUpCEO}
           setCEOReview={setCeoReview}
           comments={CommentCEO}
           setComments={setCommentCEO}
           executor={name}
           link={linkto}
           setCEONotYet={setCeoNotYet}
           setCEORejected={setRejected}
           />
      )}
        {showPopUpPeerReview && ( 
           <PeerReviewPopUp
           setShowPopUpPeerReview={setShowPopUpPeerReview}
           setPeerReview={setShowPeerReview}
           comments={CommentPeerReview}
           setComments={setCommentPeerReview}
           executor={name}
           link={linkto}
           setPeerReviewNotYet={setpeerReviewNotYet}
           />
      )}
        </div>
      )}
    </div>
  );
};

export default Archives;