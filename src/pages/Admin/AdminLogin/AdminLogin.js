// admin/Admin.js
import React, { useState } from "react";
import axios from "axios";
import { Navigate } from "react-router-dom"; // Importez le composant Navigate depuis react-router-dom
import "./AdminLogin.css"; // Importez le fichier CSS

const Admin = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [verificationCode, setVerificationCode] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [showCodePopup, setShowCodePopup] = useState(false);
  const [codeVerificationResult, setCodeVerificationResult] = useState("");
  const [verificationError, setVerificationError] = useState("");
  const [redirectToDashboard, setRedirectToDashboard] = useState(false); // Nouvel état pour la redirection

  const handleLogin = async () => {
    try {
      const response = await axios.post(
        `${process.env.REACT_APP_BACKEND_URL}/admin/login/login/admin`,
        {
          email,
          password,
        }
      );

      // Affichez le message de succès ou d'échec de la requête
      console.log(response.data.message);

      // Si la connexion est réussie, affichez le popup de code
      if (response.status === 200) {
        setShowCodePopup(true);
        setErrorMessage(""); // Réinitialisez le message d'erreur
        setCodeVerificationResult(""); // Réinitialisez le résultat de la vérification du code
        setVerificationError(""); // Réinitialisez l'erreur de vérification
      }
    } catch (error) {
      // Gérez les erreurs liées à la requête
      console.error(
        "Erreur lors de la connexion :",
        error.response.data.message
      );

      // Affichez un message d'erreur à l'utilisateur
      setErrorMessage(
        "Adresse e-mail ou mot de passe incorrect ou accès refusé"
      );
    }
  };

  const handleVerifyCode = async () => {
    try {
      const response = await axios.post(
        `${process.env.REACT_APP_BACKEND_URL}/admin/login/login/verify`,
        {
          email,
          code: verificationCode,
        }
      );

      // Affichez le message de succès ou d'échec de la vérification du code
      setCodeVerificationResult(
        response.status === 200
          ? "Code de vérification valide"
          : "Code de vérification invalide ou expiré"
      );

      // Affichez une erreur si le code est invalide
      if (response.status !== 200) {
        setVerificationError("Code de vérification incorrect ou expiré");
      } else {
        setVerificationError(""); // Réinitialisez l'erreur de vérification en cas de succès
        setShowCodePopup(false);

        // Redirigez vers la page AdminDashBoard après la vérification réussie
        setRedirectToDashboard(true);
      }
    } catch (error) {
      // Gérez les erreurs liées à la vérification du code
      console.error(
        "Erreur lors de la vérification du code :",
        error.response.data.message
      );
    }
  };

  // Si redirectToDashboard est vrai, redirigez vers la page AdminDashBoard
  if (redirectToDashboard) {
    return <Navigate to="/AdminDashBoard" />;
  }

  return (
    <div className="form-container">
      <h2 className="panel-admin">Login Admin</h2>
      <div className="form-group">
        <label className="label">Email:</label>
        <input
          type="text"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          className="input"
        />
      </div>

      <div className="form-group">
        <label className="label">Mot de passe:</label>
        <input
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          className="input"
        />
      </div>

      <button onClick={handleLogin} className="button">
        Se connecter
      </button>

      {errorMessage && <p style={{ color: "red" }}>{errorMessage}</p>}

      {showCodePopup && (
        <div className="popup">
          <h2>Code de vérification</h2>
          <label className="label">Code:</label>
          <input
            type="text"
            value={verificationCode}
            onChange={(e) => setVerificationCode(e.target.value)}
            className="input"
          />
          <button onClick={handleVerifyCode} className="button">
            Vérifier le code
          </button>
          {verificationError && (
            <p style={{ color: "red" }}>{verificationError}</p>
          )}
          {codeVerificationResult && <p>{codeVerificationResult}</p>}
        </div>
      )}
    </div>
  );
};

export default Admin;
