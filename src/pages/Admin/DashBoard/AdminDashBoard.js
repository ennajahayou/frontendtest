import React, { useState, useEffect } from "react";
import axios from "axios";
import DashboardHeader from "../../../Components/admin/DashboardHeader/DashboardHeader";
import DashboardButtons from "../../../Components/admin/DashboardButtons/DashboardButtons";
import InfoCard from "../../../Components/admin/Info/Info";
import "./AdminDashboard.css"; // Importez le fichier CSS

const AdminDashboard = () => {
  const [dioInfo, setDioInfo] = useState([]);
  const [dailyThanks, setDailyThanks] = useState([]);
  const [scenarios, setScenarios] = useState([]);
  const [selectedView, setSelectedView] = useState("dioInfo");
  const [selectedMonth, setSelectedMonth] = useState("");
  const [selectedScenarios, setSelectedScenarios] = useState([]);

  useEffect(() => {
    const fetchData = async (url, setData) => {
      try {
        const response = await axios.get(url);
        setData(response.data);
      } catch (error) {
        console.error("Erreur de requête :", error);
      }
    };

    if (selectedView === "dioInfo") {
      fetchData(
        `${process.env.REACT_APP_BACKEND_URL}/admin/dashboard/dio-info`,
        setDioInfo
      );
    }

    if (selectedView === "dailyThanks") {
      fetchData(
        `${process.env.REACT_APP_BACKEND_URL}/admin/dashboard/dailythanks/total`,
        setDailyThanks
      );
    }

    if (selectedView === "scenario") {
      fetchData(
        `${process.env.REACT_APP_BACKEND_URL}/admin/dashboard/scenarios`,
        setScenarios
      );
    }
  }, [selectedView]);

  const handleDioInfo = () => {
    setSelectedView("dioInfo");
    setSelectedMonth("");
  };

  const handleDailyThanks = () => {
    setSelectedView("dailyThanks");
  };

  const handleScenario = () => {
    setSelectedView("scenario");
  };

  const handleMonthChange = (event) => {
    const selectedMonth = event.target.value;
    setSelectedMonth(selectedMonth);
  };

  // Fonction pour gérer l'envoi des réponses cochées
  const handleSendResponses = () => {
    // Envoyez les réponses cochées à votre backend ou effectuez toute autre action nécessaire
    console.log("Réponses cochées :", selectedScenarios);
  };

  // Fonction pour gérer le changement d'état des cases à cocher
  const handleCheckboxChange = (execution, scenario) => {
    const updatedSelectedScenarios = [...selectedScenarios];
    const scenarioIndex = updatedSelectedScenarios.findIndex(
      (s) => s.execution === execution
    );

    if (scenarioIndex !== -1) {
      // Mettez à jour l'état de la case à cocher pour le scénario spécifique
      updatedSelectedScenarios[scenarioIndex][scenario] =
        !updatedSelectedScenarios[scenarioIndex][scenario];
      setSelectedScenarios(updatedSelectedScenarios);
    }
  };

  return (
    <div className="dashboard-container">
      <DashboardHeader />
      <DashboardButtons
        handleDioInfo={handleDioInfo}
        handleDailyThanks={handleDailyThanks}
        handleScenario={handleScenario}
        selectedView={selectedView}
        handleMonthChange={handleMonthChange}
        selectedMonth={selectedMonth}
      />

      {selectedView === "dioInfo" && dioInfo.length > 0 && (
        <InfoCard
          title="Informations sur les Dio"
          data={dioInfo}
          renderData={(data) => (
            <ul>
              {data.map((dio) => (
                <li key={dio.id}>
                  <strong>{dio.nom_dio}</strong> - {dio.dio_description} (
                  {dio.nombre_personnes} membres)
                </li>
              ))}
            </ul>
          )}
        />
      )}

      {selectedView === "dailyThanks" && dailyThanks.length > 0 && (
        <InfoCard
          title="Total de Thanks par jour"
          data={dailyThanks}
          renderData={(data) => (
            <ul>
              {data
                .filter(
                  (dailyThank) =>
                    new Date(dailyThank.date).getMonth() + 1 ===
                    parseInt(selectedMonth, 10)
                )
                .map((filteredDailyThank) => (
                  <li key={filteredDailyThank.date}>
                    <strong>{filteredDailyThank.date}</strong>:{" "}
                    {String(filteredDailyThank.total_thanks)} thanks
                  </li>
                ))}
            </ul>
          )}
        />
      )}

      {selectedView === "scenario" && scenarios.length > 0 && (
        <InfoCard
          title="Scénarios"
          data={scenarios}
          renderData={(data) => (
            <div>
              <table className="scenario-table">
                <thead>
                  <tr>
                    <th>Execution</th>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Scenario 1</th>
                    <th>Scenario 2</th>
                    <th>Scenario 3</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((scenario) => (
                    <tr key={scenario.execution}>
                      <td>{scenario.execution}</td>
                      <td>{scenario.code}</td>
                      <td>{scenario.name}</td>
                      <td>
                        <input
                          type="checkbox"
                          name={`selectedScenarios[${scenario.execution}][scenario1]`}
                          value={scenario.scenario1}
                          onChange={() =>
                            handleCheckboxChange(
                              scenario.execution,
                              "scenario1"
                            )
                          }
                        />
                        {scenario.scenario1}
                      </td>
                      <td>
                        <input
                          type="checkbox"
                          name={`selectedScenarios[${scenario.execution}][scenario2]`}
                          value={scenario.scenario2}
                          onChange={() =>
                            handleCheckboxChange(
                              scenario.execution,
                              "scenario2"
                            )
                          }
                        />
                        {scenario.scenario2}
                      </td>
                      <td>
                        <input
                          type="checkbox"
                          name={`selectedScenarios[${scenario.execution}][scenario3]`}
                          value={scenario.scenario3}
                          onChange={() =>
                            handleCheckboxChange(
                              scenario.execution,
                              "scenario3"
                            )
                          }
                        />
                        {scenario.scenario3}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <button onClick={handleSendResponses}>Envoyer</button>
            </div>
          )}
        />
      )}
    </div>
  );
};

export default AdminDashboard;
