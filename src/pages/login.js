import React, { Component } from "react";
import "./login.css";
import Sidebar from "../Components/Sidebar";
import Homepage from "./Homepage";
import thanksandtip from '../images/Thanksandtip.png';
import Image2 from '../images/Image2.jpg'; // Importer l'image
import Cookies from "js-cookie"; // Importez la bibliothèque js-cookie

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }

  handleEmailChange = (e) => {
    this.setState({ email: e.target.value });
  };

  handlePasswordChange = (e) => {
    this.setState({ password: e.target.value });
  };

  handleLogin = () => {
    const { email, password } = this.state;

    // Envoyer les informations d'identification au backend
    fetch(process.env.REACT_APP_BACKEND_URL + "/login/api/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email, password }),
    })
      .then((response) => {
        if (response.status === 200) {
          // La connexion est réussie, rediriger l'utilisateur
          response.json().then((data) => {
            localStorage.setItem("userId", data.userId);
            localStorage.setItem("userName", data.userName);
            localStorage.setItem("isCEO", data.isCEO);
            Cookies.set("token", data.token, { secure: true }); // Ajout du stockage du token dans un cookie sécurisé
          });

          console.log(localStorage.getItem("isCEO"));

          window.location.href = "/Homepage";
        } else {
          // Afficher un message d'erreur
          alert("Identifiant ou mot de passe incorrect.");
        }
      })
      .catch((error) => {
        console.error("Erreur lors de la connexion :", error);
      });
  };

  render() {
    return (
      <div className="login-container">
        <div className="background-image"></div>
        <div className="account">
          <p className="empower-text">Empower Talents, Investors and<br></br> Entrepreneurs to make together a better <br></br>world for all of us</p>
          <p className="TheWork-text">The Work, Investment and Rewarding model<br></br> encapsulated  in a user-friendly worldwide tech platform <br></br> for an era of Freedom, Fairness and Prosperity</p>
          <p className="ByMKIF">By MKIF</p> 
          <p className="Exclusively">Exclusively by cooptation</p>
          <p className="AlreadyMember ">Already Member ?</p>
          <p className="Thanks-text">Thanks C.I.T Cash</p>
        </div>

        <form className='forme'> 
          <div className="form-group">
            <p className="MailAdress">Your Email</p>
            <input
              type="email"
              value={this.state.email}
              onChange={this.handleEmailChange}
            />
          </div>
          <div className="form-group">
            <p className="Password">Your Password</p>
            <input
              type="password"
              value={this.state.password}
              onChange={this.handlePasswordChange}
            />
          </div>
          <button className="buttonLogin" type="button" onClick={this.handleLogin}>
            Welcome ➡
          </button>
        </form>
      </div>
    );
  }
}

export default LoginPage;
