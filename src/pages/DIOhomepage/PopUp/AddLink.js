import axios from "axios";
import { useState, useContext, useEffect, useRef } from "react";
import "./Work.css";

const AddLinkPopUp = ({
  setShowPopUpLink,
  setIsCreatingExecution,
  setSelfReview,
  setExecutionId,
  initialWorkText, // Renamed the prop to initialWorkText
  setWorkText,
  link,
  setLink
}) => {
  // TODO: add real information in jsonData

  const handleClick = () => {
    if (link.trim() !== "") {
    setShowPopUpLink(false);
    setSelfReview(true);
    setExecutionId(0);
  } else {
    alert("Please document your work");
  }
  };
  const [workText, setLocalWorkText] = useState(initialWorkText);
  const [file, setFile] = useState(null); // State to store uploaded file
  const [linkInputVisible, setLinkInputVisible] = useState(false); // State to manage link input visibility



  const handleFileChange = (event) => {
    const selectedFile = event.target.files[0];
    setFile(selectedFile);
  };

  const handleLinkChange = (event) => {
    setLink(event.target.value);
  };

  const handleAddFilesClick = () => {
    // Trigger file input click
    const fileInput = document.getElementById("fileInput");
    if (fileInput) {
      fileInput.click();
    }
  };

  const handleAddLinkClick = () => {
    setLinkInputVisible(true);
  };

  const handleInputChange = (e) => {
    const inputValue = e.target.value;
    // Vérifie si la longueur du texte est inférieure ou égale à 150 caractères
    if (inputValue.length <= 150) {
      setWorkText(inputValue);
    } else if (inputValue.length > 150) {
      // If the input exceeds 150 characters, truncate the input to 150 characters
      e.preventDefault();
    }
  };

  const popUpRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (popUpRef.current && !popUpRef.current.contains(event.target)) {
        setShowPopUpLink(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [setShowPopUpLink]);

  return (
    <div ref={popUpRef} className="submition-pop-up-link">
    <h2>Document your work</h2>
<div>
<h3>Add Link</h3>
<input
  className="evaluation-textarea_link"
  type="link"
  placeholder="To your cloud folder, Git, figma, website, whatever you use to share document and knowledge  "
  value={link}
  onChange={(e) => {
    setLink(e.target.value);
  }}
/>
<h3>Good to know, sharing proof of work with your Team bring to you 45% additional Thanks !</h3>

</div>
    <button
      className="evaluation-button1"
      onClick={handleClick}
    >
      I want my Thanks
    </button>
    
    </div>
  );
};

export default AddLinkPopUp;
