import axios from "axios";
import { useState, useContext, useEffect, useRef } from "react";
import "./Work.css";

const SubmitionPopUp = ({
  setShowPopUpLink,
  setShowPopUpWork,
  setIsCreatingExecution,
  setSelfReview,
  setExecutionId,
  initialWorkText, // Renamed the prop to initialWorkText
  setWorkText,
}) => {
  // TODO: add real information in jsonData

  const handleClick = () => {
    if (workText.trim() !== "") {
    setShowPopUpWork(false);
    setShowPopUpLink(true);
  } else {
    alert("Please explain how did you do.");
  }
  };
  const [workText, setLocalWorkText] = useState(initialWorkText);
  const [file, setFile] = useState(null); // State to store uploaded file
  const [linkInputVisible, setLinkInputVisible] = useState(false);
 // State to manage link input visibility



  const handleFileChange = (event) => {
    const selectedFile = event.target.files[0];
    setFile(selectedFile);
  };



  const popUpRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (popUpRef.current && !popUpRef.current.contains(event.target)) {
        setShowPopUpWork(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [setShowPopUpWork]);




  return (
    <div ref={popUpRef} className="submition-pop-up-work">
    <h2>How did you do and what result you obtained ?</h2>

    <input
        id="fileInput"
        type="file"
        style={{ display: "none" }}
        onChange={handleFileChange}
      />
<textarea
  className="evaluation-textarea"
  placeholder="If you don’t showcase your work, you lose 30% of your Thanks"
  value={workText}
  onChange={(e) => {
    setLocalWorkText(e.target.value);
    setWorkText(e.target.value); // Update the parent's state
  }}
  onKeyDown={(e) => {
    if (e.key === 'Enter' && !e.shiftKey) {
      e.preventDefault();
      const { selectionStart, selectionEnd } = e.target;
      const newText =
        workText.substring(0, selectionStart) +
        '\n' +
        workText.substring(selectionEnd);

      setLocalWorkText(newText);
      setWorkText(newText); // Update the parent's state

      e.target.setSelectionRange(selectionStart + 1, selectionStart + 1);
    }
  }}
/>


    <button
      className="evaluation-button1"
      onClick={handleClick}
    >
      Next
    </button>

    </div>
  );
};

export default SubmitionPopUp;
