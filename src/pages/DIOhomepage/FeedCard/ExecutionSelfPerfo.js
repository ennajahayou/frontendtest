import React ,{ useState ,useEffect } from "react";
import { Link } from "react-router-dom";
import "./ExecutionSelfPerfo.css";
import personna from '../../../images/icones/personna.png';
import useCountdown from "./../useCountdown";
import logo5 from "../../../images/logo5.png";
import axios from "axios";
//import socket from "../../../socket";

const ExecutionInReview = ({ id, description, talent ,status ,comments,selfDifficulty ,
  selfReactivity,clickreview ,showceopop , ceo_comments ,peerReviewers_ids,peerReview_comments, peerReview_expectations , peerReview_reactivity , peerReview_notYet_comments ,ceo_expectations, 
  ceo_reactivity ,remainingTime,link , achievement_date ,ceoFeedback_notYet,ceoFeedback_rejected ,thanks }) => {
  
   /* const [remainingtime, setRemainingTime] = useState(remainingTime);

    useEffect(() => {
      socket.on("remainingTimeUpdated", (updatedRemainingTime) => {
        setRemainingTime(updatedRemainingTime);
      });
  
      // Clean up the socket listener when component unmounts
      return () => {
        socket.off("remainingTimeUpdated");
      };
    }, []);*/
  
  const DEADLINES = {
    DEAD1: 24,
    DEAD2: 48,
    DEAD3: 72,
  };
  const ExC =0.1
  const ExCP =0.05

  let departHours =24; // Initialiser à une valeur par défaut

  const [showCountdown, setShowCountdown] = useState(true);
  const [showThanks, setShowThanks] = useState(true);
  //let { hours, minutes, seconds } = useCountdown(3600*departHours);
  const complexity =['Simple','Moderate','Complex ','Highly Complex']
  const repetition =[ 'Routine','Occasionally Repeated', 'Rare' ,'Novel']
  const result_quality =[ 'Basic','Good', 'High' ,' Outstanding']
  const reactivity =['Responsive','Proactive','Agile','Exceptional']
  const values = [1, 4, 10, 20]
  departHours=values[selfDifficulty]*values[selfReactivity]
  departHours=Math.ceil(departHours*(1 + ExC + ExCP))


  if (departHours > 6) {
    if ((values[selfDifficulty] === 4 && values[selfReactivity] === 4)) {
      departHours = DEADLINES.DEAD1;
    } else if (values[selfDifficulty] === 10 || values[selfReactivity] === 10) {
      departHours = DEADLINES.DEAD2 ;
    } else if (values[selfDifficulty] === 20 || values[selfReactivity] === 20) {
      departHours = DEADLINES.DEAD3 ;
    };
  } else{
    thanks=departHours;
  }


  const { hours, minutes, seconds } = useCountdown(remainingTime);

  useEffect(() => {
    if (departHours < 6 && remainingTime == null ){
      setShowCountdown(false);
      setShowThanks(true);
    } else {
      setShowCountdown(true);
      setShowThanks(false);
    }
  }, [departHours]);

  const [showDetails, setShowDetails] = useState(false);

  const [showPeerReview, setShowPeerReview] = useState(false);
  const divStyle = { // Taille de police plus grande
    fontWeight: 'bold',
    justifyContent :'center',
    alignItems: 'center'// Poids de police plus épais
  };



  const handlePeerReviewClick = () => {
    setShowPeerReview(true);
  };
  const userName = localStorage.getItem('userName');
  const userId = localStorage.getItem('userId');


  const toggleDetails = () => {

    setShowDetails(!showDetails);
  };
  let userIdInt = parseInt(userId, 10);
  let parsedPeerReviewIds = [];
if (peerReviewers_ids && typeof peerReviewers_ids === 'string' && peerReviewers_ids.trim() !== '') {
  parsedPeerReviewIds = peerReviewers_ids.split(',').map(id => parseInt(id.trim(), 10));
}



  
  return (
    <div>
    <div className="message bubble">
      <div className="first">
      <div className="left">
      <div className="first-row">
      <div className="creator"><div>Creator :</div><div style={divStyle}>{talent}</div> </div>
      <div className="statut">Status :<div style={divStyle}>  {status}</div></div>
      <div className="count-down" >{status ==="In review"  ? ( 
          <p >Review Countdown <div style={divStyle}>{`${hours}H:${minutes}Mn`}</div></p>
      ) : status ==="Achieved"  ?(
          <p >Achievement Date :<div style={divStyle}>{achievement_date.substring(0,10)}</div></p>
       ):status ==="Open" ?(<p >No Countdown for<div style={divStyle}>Open</div></p>
       ):status ==="Rejected" ?(<p >Date of Closing :<div style={divStyle}></div></p>
       ):(<p >Due Date :<div style={divStyle}></div></p>)}</div>
      </div>
      <div className="description-diopage">
      <div>Execution description :<div style={divStyle}>{description}</div></div>
      </div>
      </div>
      <div className="right">
        <img className="profile" src={personna} />
        <div className="name"><div>Doer :</div> <div style={divStyle}> {talent} </div></div>
        <div className="thanks-score">
        {status ==="In review"  ? ( 
          <p>Thanks :<div style={divStyle}> Not Yet</div></p>
      ) : status ==="Achieved"  ?(
        <p>Thanks :<div style={divStyle}>{thanks}</div>  <img className="symbole_th" src={logo5} /></p>
       ):(<p>Thanks :<div style={divStyle}> Not Yet</div></p>)}</div>

      </div>
      </div>
      <div className="second">
      {showDetails && (
      <div className="additional-info">

      <div className="add-first">
      <div className="left">
      <div className="second-row">
      {comments.trim().length === 0  ?(<div className="comments"><p>No Executor Peroformer Comment</p></div>):( <div className="comments">
      <div>How :</div><div style={divStyle}> {comments} </div> 
      </div>)}
          
      <div className="doc">
  <div>
    {link !== null && link.trim().length !== 0 ? (
      <p>Documentation: <a href={link} target="_blank">Clic here</a>.</p>
    ) : (
       <p>No link uploaded</p>
    )}
  </div>
</div>
      </div>
      </div>
      {status ==="Achieved"  || status === "Rejected" || talent === userName ? ( 
        <div className="auto-eval"><div>Auto Evaluation:</div>
        <div className="dif-rea">Complexity :<div style={divStyle}>{complexity[selfDifficulty]}</div></div>
        <div className="dif-rea">Repetition: <div style={divStyle}>{repetition[selfReactivity]}</div> </div>

      </div>
                  ):(<></>)}
      </div>

      {status ==="Achieved"  || status === "Rejected"? ( 
      <div className="add-second">
        
          {ceo_comments === '' && ceo_comments !== null ?(<div className="ceo_comments"><p>No CEO FeedBack for this execution</p></div>):ceo_comments !== '' && ceo_comments !== null ?(           <div className="ceo_comments">
          <div>CEO Evaluation Comment :<div style={divStyle}> {ceo_comments} </div> </div>  </div>):(<></>)}

          {ceo_expectations !==null && ceo_reactivity!==null  && (      
                  <div className="ceo-eval"><div>CEO Evaluation:</div>
                  <div className="dif-rea"> Result Quality :<div style={divStyle}> {result_quality[ceo_expectations]} </div></div>  
                  <div className="dif-rea">Reactivity :<div style={divStyle}> {reactivity[ceo_reactivity]} </div></div>
                  </div>)}
        {status === "On going"  &&(<div className="ceo-eval">CEO Evaluation (NotYet) :<div style={divStyle}> {ceoFeedback_notYet}</div> </div>)}
        {(status === "Rejected" || status === "Open")  &&(<div className="ceo-eval">CEO Evaluation (Rejected) :<div style={divStyle}> {ceoFeedback_rejected}</div> </div>)}
      </div>
      ):(<></>)}
      
      {status ==="Achieved"  || status === "Rejected"? ( 
      <div className="add-second">
  {/* Vérifier si peerReview_comments existe et n'est pas null avant de traiter */}
  { typeof peerReview_comments === 'string'  && (
    <div className="peer_review_comment">
        {peerReviewers_ids.split(',').map((comment, index) => (
                peerReview_comments.split(',')[index].trim().length === 0 || peerReview_comments === "" ?(<div className="comments-peer"><p>No Feedback for Anonyme PeerReview {index + 1}</p></div>):(               
                  <div className="comments-peer">
                Anonyme PeerReview FeedBack {index + 1}:
          <div key={`peerComment_${index}`} style={divStyle}>
             {peerReview_comments.split(',')[index].trim()}
          </div>
          </div>)
        ))}
    </div>
  )}

  {/* Vérifier si peerReview_expectations et peerReview_reactivity sont des chaînes valides avant de traiter */}
  {peerReview_expectations && typeof peerReview_expectations === 'string' && peerReview_reactivity && typeof peerReview_reactivity === 'string' && peerReview_expectations.length > 0 && peerReview_reactivity.length > 0 && (

<div className="peer_review_eva">
        {peerReview_expectations.split(',').map((expectation, index) => (
          peerReview_reactivity.split(',')[index] && (
            <div className="eval-peer">
              PeerReview Evaluation {index + 1}: 
            <div className="peerExpectation" key={`peerExpectation_${index}`} >
            <div className="dif-rea">Result Quality :<div style={divStyle}>  {result_quality[expectation.trim()]} </div> </div>
            <div className="dif-rea"> Reactivity:<div style={divStyle}> {reactivity[peerReview_reactivity.split(',')[index].trim()]}</div></div>
            </div>
            </div>
          )
        ))}
    </div>
  )}
</div>
      ):(<></>)}

{typeof peerReview_notYet_comments === 'string' && (status ==="Achieved"  || status === "Rejected") && (
  <div className="peer_review_comment">
    {peerReview_notYet_comments.split(',').map((comment, index) => (
      peerReview_notYet_comments && peerReview_notYet_comments.trim().length > 0 ? (
        <div className="comments-peer" key={`peerComment_${index}`}>
          Anonyme PeerReview FeedBack NotYet {index + 1}:
          <div style={divStyle}>
            {comment.trim()}
          </div>
        </div>
      ) : (
        <div className="comments-peer" key={`peerComment_${index}`}>
          <p>No Feedback for Anonyme PeerReview {index + 1}</p>
        </div>
      )
    ))}
  </div>
)}



      </div>
      )}

      <button className="plus"  onClick={toggleDetails} style={{ fontSize: "2vw" }}>
        {showDetails   ? '-' : '+'}
      </button>
      {localStorage.getItem("isCEO") === "1" ? (
  <>
    {ceo_comments === null && status === "In review" && talent !== userName && (
      <button className="review" onClick={() => showceopop(id)}>
        <div style={divStyle}> Evaluate it </div>
      </button>
    )}
    {status === "Open" && talent !== userName && (
      <button className="review">
        <div style={divStyle}> I want to do it </div>
      </button>
    )}
    {status === "On going" && talent === userName && (
      <button className="review">
        <div style={divStyle}> Submit my work </div>
      </button>
    )}
  </>
) : (
  <>
    {  !parsedPeerReviewIds.includes(userIdInt)  && status === "In review" && talent !== userName && (
      <button className="review" onClick={() => clickreview(id)}>
        <div style={divStyle}> Make a review</div>
      </button>
    )}
        {  parsedPeerReviewIds.includes(userIdInt)  && status === "In review" && talent !== userName && (

        <div style={divStyle}></div>

    )}
    {status === "Open" && talent !== userName && (
      <button className="review">
        <div style={divStyle}> I want to do it</div>
      </button>
    )}
    {status === "On going" && talent === userName && (
      <button className="review">
        <div style={divStyle}> Submit my work</div>
      </button>
    )}
  </>
)}

      </div>
    </div>

    </div>
  );
};
export default ExecutionInReview;