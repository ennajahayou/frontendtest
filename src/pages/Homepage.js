import React, { useState, useEffect } from "react";
import "./Homepage.css";
import Sidebar from "../Components/Sidebarhomepage";
import DIOList from "../Components/DIOList";
import Wallet from "../Components/Wallet";
import axios from "axios";

import logo3 from '../images/logo3.png';

const Homepage = () => {
const [dioData, setDioData] = useState([]);

useEffect(() => {
  axios.get(process.env.REACT_APP_BACKEND_URL + "/DIO/dioData")
  .then((res) => {
    const formattedData = res.data.map(item => ({
      id: item.id,
      name: item.nom_dio,
      description: item.dio_description
    }));
    setDioData(formattedData);
  });
}, []);


return (
  <div className="App">
    <Sidebar />
    <div className="main-content">
      <Wallet  />
      <h4 className="titre">My Digital Twin Organizations (DTO)</h4>
      <div className="diolist-container">
      <DIOList className="diolist" dioData={dioData} />
      </div>
    </div>
  </div>
  );
};


export default Homepage;
