import React, { useState } from 'react';

import { useContext } from 'react';
import { TasksContext } from '../TasksContext';

import './Co-opt.css';
import Sidebar from '../../Components/SidebarDIO';
import Wallet from "../../Components/Wallet";




const Coopt = () => {

    return (
    <div className="container">
        <Sidebar />
        <div className="main-content">
        <Wallet  />
        <div className="execution-board">
          <h1>DTO Thanks C.I.T Cash</h1>
          </div>

        <div className='actions'>
        Co-opt
        <p>
        If you want to co-opt talent, create their new account {" "}
            <a href="./signup">here</a>.
          </p>
        </div>
        </div>
    </div>
    );
}

export default Coopt;