import "./PeerReviewNotYet.css";
import { useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";

const PeerReviewNotYet = ({ executionId, setShowEvaluation ,setShowPopUpPeerReview ,comments , feedback, setFeedback}) => {
  const [currentQuestion, setCurrentQuestion] = useState(1);
  const [expectations, setExpectations] = useState(0);

  const handleExpectationsClick = (index) => {
    setExpectations(index);
    setCurrentQuestion(2);
  };
  const token = Cookies.get("token");



  const handleSubmit = (index) => {
    const data = {
      executionId: executionId,
      feedback: feedback,
    };
    if (feedback.trim() !== "") {
    axios.post(process.env.REACT_APP_BACKEND_URL + "/execution/peerReview_NotYet", data, {
      headers: {
        Authorization: `Bearer ${token}`, // Ajouter le token aux en-têtes
      },
    });
    window.location.reload()
  } else {
    alert("Please explain why not yet.");
  }

  };



  return (
    <div className="evaluation-container">
          <h3 >Finally</h3>
          <h3 className="better">What would it take to reach your expectations ? </h3>
            <input className="input-feed"  placeholder="A constructive feedback makes your teammate better and increase your collective odd of success"   value={feedback}
            onChange={(e) => setFeedback(e.target.value)}></input>
          <button
            className="feedback"
            onClick={() => handleSubmit() }
            //setShowEvaluation(false)}
          >
            Share  ➡
          </button>


    </div>          //onClick={() => window.location.reload()}
  );
};

export default PeerReviewNotYet;
