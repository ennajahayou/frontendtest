import axios from "axios";
import Cookies from "js-cookie";
const ExecutionInReview = ({
  execution,
  handleDropClick,
  handleThanksClick,
  droppedTaskIndex,
}) => {
  const setExecutionDone = () => {
    const token = Cookies.get("token");
    axios
      .post(process.env.REACT_APP_BACKEND_URL + "/executionBoard/setDone", {
        executionId: execution.id,
        userId: localStorage.getItem("userId"),
      },{
        headers: {
          Authorization: `Bearer ${token}`, // Ajouter le token aux en-têtes
        },
      })
      .then((res) => {
        if (res.status === 200) {
          if (res.data.scoreThanks > 0) {
            alert("You earned " + res.data.scoreThanks + " thanks");
          } else {
            alert(res.data);
          }
        }
      });
  };

  const formattedDeadline = new Date(execution.deadline).toLocaleDateString();
  return (
    <div className="execution" key={execution.id}>
      <div>
        <b>{execution.exec_description}</b>
      </div>
      <div>In review</div>
      <div>Deadline for reviews : {formattedDeadline}</div>
      <button className="accept-button" onClick={setExecutionDone}>
        Set Done
      </button>
    </div>
  );
};

export default ExecutionInReview;
