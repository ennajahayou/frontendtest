import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./Sidebarhomepage.css";

import personna from '../images/icones/personna.png';


import logo from "../images/logo.png";
const userName = localStorage.getItem("userName");

const Sidebar = () => {
  const [activeSection, setActiveSection] = useState(null);
  return (
  <div className="sidebarhome">
        <Link to="/Homepage" className={`sidebar-section-logo ${activeSection === "/Homepage" ? 'active' : ''}`} >
        <div className="logo">Thanks C.I.T Cash </div>
    <span>by MKIF</span>
    </Link>
    <button className="sidebar-section"><span>My DTO's</span></button>
    <button className="sidebar-section"><span>Create a new DTO</span></button>
    <button className="sidebar-section"><span>Discover DTOs</span></button>
    <button className="sidebar-section-sponsorize"><span>Sponsorize a DTO Creator</span></button>
    <Link to="/Myprofile" className={`sidebar-section-link ${activeSection === "/Myprofile" ? 'active' : ''}`} >
     <span>My profile</span> 
    </Link>
  </div>
  )
};

export default Sidebar;
