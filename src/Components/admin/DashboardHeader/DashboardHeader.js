import React from "react";
import { Link } from "react-router-dom";
import "./DashbordHeader.css";

const DashboardHeader = () => {
  return (
    <div>
      <h2>Tableau de Bord Admin</h2>
      <Link to="/" className="back-to-home-button">
        Retour à l'accueil
      </Link>
    </div>
  );
};

export default DashboardHeader;
