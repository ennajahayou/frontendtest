// DashboardButtons.js
import React from "react";
import "./DashboardButtons.css";

const DashboardButtons = ({
  handleDioInfo,
  handleDailyThanks,
  handleScenario,
  selectedView,
  handleMonthChange,
  selectedMonth,
}) => {
  return (
    <div className="button-container">
      <button onClick={handleDioInfo}>Nombre de DTO inscrits</button>
      <button onClick={handleDailyThanks}>Thanks par jour</button>
      <button onClick={handleScenario}>Choix de scénario</button>
      {selectedView === "dailyThanks" && (
        <select
          onChange={handleMonthChange}
          value={selectedMonth}
          className="month-selector"
        >
          <option value="">Sélectionnez un mois</option>
          <option value="1">Janvier</option>
          <option value="2">Février</option>
          <option value="3">Mars</option>
          <option value="4">Avril</option>
          <option value="5">Mai</option>
          <option value="6">Juin</option>
          <option value="7">Juillet</option>
          <option value="8">Août</option>
          <option value="9">Septembre</option>
          <option value="10">Octobre</option>
          <option value="11">Novembre</option>
          <option value="12">Décembre</option>
        </select>
      )}
    </div>
  );
};

export default DashboardButtons;
