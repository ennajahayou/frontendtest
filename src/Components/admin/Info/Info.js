// InfoCard.js
import React from "react";
import "./Info.css";

const InfoCard = ({ title, data, renderData }) => {
  return (
    <div className="info-card">
      <h3>{title}</h3>
      {renderData(data)}
    </div>
  );
};

export default InfoCard;
