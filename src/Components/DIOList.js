import React from "react";
import { Link } from "react-router-dom";
import "./DIOList.css";
import { useContext } from "react";
import { TasksContext } from "../pages/TasksContext";

const DIOList = ({ dioData }) => {
  const { selectDioId } = useContext(TasksContext);

  const handleDIOClick = (dioId) => {
    selectDioId(dioId);
  };
  return (
  <div className="dio-list">
    {dioData.map((dio, index) => (
      <div key={dio.id} className="dio-item">
        <div className="dio-text">
          <h3>{dio.name}</h3>
          <p>{dio.description}</p>
        </div>
        <Link to={`/DIO`}>
          <button className="participate-button" onClick={() => handleDIOClick(dio.id)}>Come in</button>
        </Link>
      </div>
    ))}
  </div>
  );
};

export default DIOList;
